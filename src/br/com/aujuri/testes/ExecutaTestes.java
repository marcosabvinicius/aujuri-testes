package br.com.aujuri.testes;



public class ExecutaTestes {
	public static void main(String[] args) {
		
		TestaCadastroFuncionarioChrome tf = new TestaCadastroFuncionarioChrome();
		tf.inicializa();
		tf.acessaFormulario();
		tf.cadastraFuncionario();
		tf.finaliza();
		
		TestaPessoaFisicaChrome tc = new TestaPessoaFisicaChrome();
		tc.inicializa();
		tc.acessaFormulario();
		tc.cadastraCliente();
		tc.finaliza();
		
		TestaPessoaJuridicaChrome tj = new TestaPessoaJuridicaChrome();
		tj.inicializa();
		tj.acessaFormulario();
		tj.cadastraCliente();
		tj.finaliza();
		
		
		
	}
}
