package br.com.aujuri.testes;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestaCadastroFuncionarioChrome {

	/*
	* o driver eh global, pois todos os metodos devem acessar esse objeto e suas funcoes
	* melhorar esse  codigo colocando isso em uma classe abstrata ou ate mesmo em uma interface.
	*/
	WebDriver driver;
	
	@Before
	public void inicializa(){
		System.setProperty("webdriver.chrome.driver", "exe\\chromedriver.exe");
		driver = new ChromeDriver();
		//acessar um site		
		driver.get("http://localhost:8080/aujuri/");
		System.out.println("Iniciando browser ..");
		driver.manage().window().maximize();
	}
	
	public void acessaFormulario() {
		WebElement funcionario = driver.findElement(By.id("funcionario"));
		funcionario.click();
		System.out.println("Clicando na op�opcaoo cliente da funcionario ..");
		
		WebElement linkCadastrar = driver.findElement(By.id("cadastrarFuncionario"));
		linkCadastrar.click();
		System.out.println("Clicando na opcao cliente da pessoa cadastrar cliente ..");
	}
	
	@Test
	public void cadastraFuncionario(){
		
		//Usei o ExplicittWait para que meu c�digo espere ate 10 segundos pelo formulario
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("sobrenomeF")));
		
		WebElement campoNome = driver.findElement(By.name("nome"));
		campoNome.sendKeys("Marcos");
		System.out.println("colocando nome ..");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoSobrenome = driver.findElement(By.id("sobrenomeF"));
		campoSobrenome.sendKeys("da Silva");
		System.out.println("colocando sobrenome");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoDtNascimento = driver.findElement(By.id("dataNasc"));
		campoDtNascimento.sendKeys("10/11/1991");
		System.out.println("colocando data de nascimento");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoRg = driver.findElement(By.id("rg"));
		campoRg.sendKeys("7845845647");
		System.out.println("colocando rg");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoCpf = driver.findElement(By.id("cpf"));
		campoCpf.sendKeys("78945612389");
		System.out.println("colocando cpf");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoOab = driver.findElement(By.id("oab"));
		campoOab.sendKeys("1234567");
		System.out.println("colocando numero oab");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoCargo = driver.findElement(By.id("formCargo"));
		campoCargo.sendKeys("Advogado");
		System.out.println("colocando cargo");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoRua = driver.findElement(By.id("rua"));
		campoRua.sendKeys("Rua Afonso");
		System.out.println("colocando rua");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoNumero = driver.findElement(By.id("numero"));
		campoNumero.sendKeys("10 B");
		System.out.println("colocando numero");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoCep = driver.findElement(By.id("cep"));
		campoCep.sendKeys("78945789");
		System.out.println("colocando cpf");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoBairro = driver.findElement(By.id("bairro"));
		campoBairro.sendKeys("Centro");
		System.out.println("colocando bairro");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoCidade = driver.findElement(By.id("cidade"));
		campoCidade.sendKeys("Centro");
		System.out.println("colocando bairro");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoEstado = driver.findElement(By.id("formEstado"));
		campoEstado.sendKeys("Sao Paulo");
		System.out.println("colocando estado");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoDdd = driver.findElement(By.id("ddd"));
		campoDdd.sendKeys("011");
		System.out.println("colocando ddd");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoTelefone = driver.findElement(By.id("telefone"));
		campoTelefone.sendKeys("123456789");
		System.out.println("colocando telefone");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoEmail = driver.findElement(By.id("email"));
		campoEmail.sendKeys("teste@teste.com.br");
		System.out.println("colocando email");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoSenha = driver.findElement(By.id("senha"));
		campoSenha.sendKeys("teste123");
		System.out.println("colocando senha");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement btnCadastrar = driver.findElement(By.id("btnCadastrar"));
		btnCadastrar.click();
		System.out.println("Clicando no botao cadastrar");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	@After
	public void finaliza(){
		System.out.println("==================ACABOU====================");
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Browser fechando em 3");
		System.out.println("Browser fechando em 2");
		System.out.println("Browser fechando em 1");
		driver.close();
	}
	
	public static void main(String [] args) {
		TestaCadastroFuncionarioChrome tf = new TestaCadastroFuncionarioChrome();
		tf.inicializa();
		tf.acessaFormulario();
		tf.cadastraFuncionario();
		tf.finaliza();
	}//main
		
}