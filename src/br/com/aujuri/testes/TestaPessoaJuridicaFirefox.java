package br.com.aujuri.testes;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestaPessoaJuridicaFirefox {
	//global, pois todos os m�todos devem acessar esse objeto e suas fun��es ..
	WebDriver driver;
	
	@Before
	public void inicializa(){
		System.setProperty("webdriver.gecko.driver", "exe\\geckodriver.exe");
		driver = new FirefoxDriver();
		//acessar um site		
		driver.get("http://127.0.0.1:8080/aujuri");
		System.out.println("Iniciando browser ..");
		driver.manage().window().maximize();
	}
	
	@Test
	public void acessaFormulario() {
		WebElement cliente = driver.findElement(By.id("cliente"));
		cliente.click();
		System.out.println("Clicando na op��o cliente da cliente ..");
		
		//botao novo do menu configuracao		
		WebElement linkPessoaJuridica = driver.findElement(By.id("pessoaJuridica"));
		linkPessoaJuridica.click();
		System.out.println("Clicando na op��o cliente da pessoa juridica ..");
		
		
		WebElement linkCadastrar = driver.findElement(By.id("cadastrarClientePJ"));
		linkCadastrar.click();
		System.out.println("Clicando na op��o cliente da pessoa cadastrar cliente ..");
		
		//Usei o implictWait para que meu c�digo espere at� 10 segundos pelo formul�rio
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}
	
	@Test
	public void cadastraCliente(){
		//Usei o ExplicittWait para que meu c�digo espere at� 10 segundos pelo formul�rio
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("razaoSocial")));
		
		WebElement campoRazaoSocial = driver.findElement(By.name("razaoSocial"));
		campoRazaoSocial.sendKeys("Marcos Vinicius ME"); 
		System.out.println("colocando razao social ..");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoCNPJ = driver.findElement(By.id("cnpj"));
		campoCNPJ.sendKeys("12345678914785");
		System.out.println("colocando sobrenome");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoContato = driver.findElement(By.id("contato"));
		campoContato.sendKeys("Marcos da Silva");
		System.out.println("colocando nome de contato");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoDDD = driver.findElement(By.id("ddd"));
		campoDDD.sendKeys("011");
		System.out.println("colocando ddd");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoTelefone = driver.findElement(By.id("telefone"));
		campoTelefone.sendKeys("12347856");
		System.out.println("colocando telefone");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoEmpresa = driver.findElement(By.id("empresa"));
		campoEmpresa.sendKeys("Softare Express");
		System.out.println("colocando empresa");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		WebElement campoCep = driver.findElement(By.id("cep"));
		campoCep.sendKeys("78945789");
		System.out.println("colocando cpf");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoRua = driver.findElement(By.id("rua"));
		campoRua.sendKeys("Rua 12 de Maio");
		System.out.println("colocando rua");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WebElement campoNumero = driver.findElement(By.id("numero"));
		campoNumero.sendKeys("12 A");
		System.out.println("colocando numero");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoBairro = driver.findElement(By.id("bairro"));
		campoBairro.sendKeys("Centro");
		System.out.println("colocando bairro");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoEstado = driver.findElement(By.id("formEstado"));
		campoEstado.sendKeys("S�o Paulo");
		System.out.println("colocando estado");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		WebElement campoCidade = driver.findElement(By.id("cidade"));
		campoCidade.sendKeys("Centro");
		System.out.println("colocando bairro");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		WebElement campoEmail = driver.findElement(By.id("email"));
		campoEmail.sendKeys("teste@teste.com.br");
		System.out.println("colocando email");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement btnCadastrar = driver.findElement(By.id("btnCadastrar"));
		btnCadastrar.click();
		System.out.println("Clicando no botao cadastrar");
		System.out.println("colocando email");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	@After
	public void finaliza(){
		System.out.println("==================ACABOU====================");
		System.out.println("Browser fechando em 3");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Browser fechando em 2");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Browser fechando em 1");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.close();
	}
	
	public static void main(String [] args) {
		TestaPessoaJuridicaFirefox tj = new TestaPessoaJuridicaFirefox();
		tj.inicializa();
		tj.acessaFormulario();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tj.cadastraCliente();
		tj.finaliza();
	}//main
	
	
}
