package br.com.aujuri.testes;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestaLoginChrome {

	/*
	* o driver � global, pois todos os m�todos devem acessar esse objeto e suas fun��es
	* melhorar esse c�digo colocando isso em uma classe abstrata ou at� mesmo em uma interface.
	*/
	WebDriver driver;
	
	@Before
	public void inicializa(){
		System.setProperty("webdriver.chrome.driver", "exe\\chromedriver.exe");
		driver = new ChromeDriver();
		//acessar um site		
		driver.get("http://localhost:8080/aujuri/");
		System.out.println("Iniciando browser ..");
		driver.manage().window().maximize();
	}
	
	@Test
	public void realizaLogin(){
		
		//Usei o ExplicittWait para que meu c�digo espere at� 10 segundos pelo formul�rio
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("username")));
		
		WebElement campoUsername = driver.findElement(By.name("username"));
		campoUsername.sendKeys("teste@teste.com.br");
		System.out.println("colocando login ..");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement campoPassword = driver.findElement(By.id("password"));
		campoPassword.sendKeys("teste123");
		System.out.println("colocando password");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WebElement btnLogar = driver.findElement(By.id("btnLogar"));
		btnLogar.click();
		System.out.println("Clicando no botao login");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	}//realizaLogin
	
	@After
	public void finaliza(){
		System.out.println("==================ACABOU====================");
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Browser fechando em 3");
		System.out.println("Browser fechando em 2");
		System.out.println("Browser fechando em 1");
		driver.close();
	}//finaliza
	
}
